Techturbid Terraform Registry - cert-manager
---
#### Cert-Manager module used by techturbid's GKE modules like gke_base

By default this module will download and install cert-manager from Jetstack

It will also configure credentials and domains to create the DNS records required by LetsEncrypt 
 
---

The input required for this modules are:

|            **Variable**           |       **Value**       |
|-----------------------------------|:----------------------|
| CLUSTER_NAME                      |                       |
| SERVICE-ACCOUNT_KEY                      |                       |
| CLOUDDNS_PROJECT                      |                       |

--- 
 
The variables with default values are:

|             **Variable**           |       **Value**       |
|------------------------------------|:----------------------|
| GCP_DNS_PROJECT          | techtur-bid-commons      |
| CERTIFICATE_MANAGER_NAMESPACE          | istio-system      |
| techturbidDOMAIN_NAME          | techtur.bid      |
| LETS_ENCRYPT_EMAIL          | gcoutinho@techturbid.com      |

---

This module will output the following values:

|              **Output**            |                       **Value**               |
|------------------------------------|:----------------------------------------------|
| cert-manager-crds                 |  helm_release.cert-manager-crds.id     |
| cert-manager                |  helm_release.cert-manager.id     |
| cert-manager-ca                 |  helm_release.cert-manager-ca.id     |

---

This module requires the following providers:
```hcl-terraform
provider "google" {
  credentials = var.service-account_key
  project     = var.gcp_project
  region      = var.cluster_region
}

provider "kubernetes" {
  load_config_file = false
  host = "https://${module.gke.cluster_endpoint}"
  cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
  token = data.google_client_config.google-beta_current.access_token
}

provider "helm" {
  install_tiller  = true
  tiller_image    = "gcr.io/kubernetes-helm/tiller:v${var.tiller_version}"
  service_account = var.kubernetes_service_account_name
  namespace       = var.kubernetes_service_account_namespace

  kubernetes {
    load_config_file = false
    host = "https://${module.gke.cluster_endpoint}"
    cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
    token = data.google_client_config.google-beta_current.access_token
  }
}
```

---