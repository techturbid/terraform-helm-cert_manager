resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}


data "helm_repository" "jetstack" {
  depends_on = ["null_resource.wait_for_resources"]
  name = "jetstack"
  url  = "https://charts.jetstack.io"
}

resource "helm_release" "cert-manager-crds" {
  depends_on = ["null_resource.wait_for_resources"]
  name       = "cert-manager-crds"
  chart      = "${path.module}/cert-manager/cert-manager-crds"
}


locals {
  cluster_domain = "${var.CLUSTER_NAME}.${var.techturbidDOMAIN_NAME}"
}


resource "kubernetes_namespace" "cert-manager" {
  metadata {
    labels = {
      "cert-manager.io/disable-validation" = "true"
    }
    name = var.CERTIFICATE_MANAGER_NAMESPACE
  }
}

resource "helm_release" "cert-manager" {
  depends_on = ["helm_release.cert-manager-crds"]
  name       = "cert-manager"
  namespace  = var.CERTIFICATE_MANAGER_NAMESPACE
  repository = data.helm_repository.jetstack.metadata[0].name
  chart      = "cert-manager"
  version    = "v0.11.0"

}


resource "kubernetes_secret" "clouddns-credentials-secret" {
  metadata {
    name = "clouddns-credentials-secret"
    namespace= var.CERTIFICATE_MANAGER_NAMESPACE
  }

  data = {
    key = var.SERVICE-ACCOUNT_KEY
  }
}

//resource "local_file" "service_account_key" {
//  filename = "service_account_key.json"
//  sensitive_content = var.SERVICE-ACCOUNT_KEY
//}

resource "helm_release" "cert-manager-ca" {
  depends_on = ["helm_release.cert-manager"]
  name  = "cert-manager-ca"
  chart = "${path.module}/cert-manager/cert-manager-ac"

  set {
    name  = "gcloud.clouddns_project"
    value = var.CLOUDDNS_PROJECT
  }
  set {
    name  = "gcloud.service_account_key"
    value = "key"
  }
  set {
    name  = "techturbid.domainName"
    value = var.techturbidDOMAIN_NAME
  }
  set {
    name  = "cluster.domainName"
    value = local.cluster_domain
  }
  set {
    name  = "certManager.letsencryptEmailForRenewal"
    value = var.LETS_ENCRYPT_EMAIL
  }
}