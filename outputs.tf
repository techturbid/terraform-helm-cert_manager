output "cert-manager-crds" {
  value = helm_release.cert-manager-crds.id
}
output "cert-manager" {
  value = helm_release.cert-manager.id
}
output "cert-manager-ca" {
  value = helm_release.cert-manager-ca.id
}