variable "wait_for_resources" {}
variable "CERTIFICATE_MANAGER_NAMESPACE" {
  default = "istio-system"
}
variable "techturbidDOMAIN_NAME" {
  default = "techtur.bid"
}
variable "LETS_ENCRYPT_EMAIL" {
  default = "ac.brazil.devops@techturbid.com"
}
variable "CLUSTER_NAME" {}
variable "SERVICE-ACCOUNT_KEY" {}
variable "CLOUDDNS_PROJECT" {}